var request = require('request');
var fetch = require('node-fetch');
var exports = module.exports = {};

exports.send  = function(text,callback){

  text = text.split(" ").join("%20")

  var headers = {
      'accept': 'application/json',
      'X-Warehouse-Code': 'tel_aviv'
  };

  var options = {
      url: encodeURI('https://ops.avo.co.il/api/v1/products/search?by_company_id=129&mode=suggest&page=1&per_page=45&query='+text),
      headers: headers
  };

  request(options, callback);
}
