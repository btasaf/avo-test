const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var http = require('http');

const app=express();

app.use(bodyParser.json());


require('./routes/itemsRoute')(app);


app.use(express.static('client/build'));

app.get('*',(req, res) => {
	res.sendFile(path.resolve(__dirname, 'client', 'build','index.html'));
});

var server = http.createServer(app).listen(80);

const PORT = process.env.PORT || 5000;
app.listen(PORT);
console.log('server running on port '+PORT);
