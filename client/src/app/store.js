import { configureStore } from '@reduxjs/toolkit';
import ItemsDisplay from './../features/ItemsDisplay/ItemsDisplaySlice.js';
import languageReducer from './../features/language/languageSlice.js';

export const store = configureStore({
  reducer: {
    items: ItemsDisplay,
    lang: languageReducer,
  },
});
