import React from 'react';
import logo from './logo.svg';
import { MainPage } from './features/MainPage/MainPage.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <MainPage/>
    </div>
  );
}

export default App;
