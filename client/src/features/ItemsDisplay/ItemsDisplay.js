import React, { useState ,useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectItemsList,getItemsListAsync,selectStatus,resetList } from './ItemsDisplaySlice.js';
import { selectLanguage,selectLanguageIndex } from './../language/languageSlice.js';
import styles from './ItemsDisplay.module.css';
import {ItemBlock} from './itemBlock/ItemBlock.js';
import ClipLoader from "react-spinners/ClipLoader";

export function ItemsDisplay(props) {
  const itemsList = useSelector(selectItemsList);
  const status = useSelector(selectStatus);
  const {noItemsFoundText} = useSelector(selectLanguage).itemDisplay
  const dispatch = useDispatch();
  const [isViewOnTop,setIsViewOnTop] = useState(true)

  useEffect(() => {
    const originalStyle = window.getComputedStyle(document.body).overflow;
    document.body.style.overflow = "hidden";
    return () => (document.body.style.overflow = originalStyle);
  }, []);

  useEffect(() => {
    if(props.text.length !== 0)
      dispatch(getItemsListAsync(props.text))
    else{
      dispatch(resetList())
    }
  }, [props.text]);

  if(status === 'loading')
  return (
    <div className={styles.spinnerCon}>
      <ClipLoader/>
    </div>
  )
  if(itemsList.length === 0&&props.text.length !== 0){
    return (
      <div>{noItemsFoundText}</div>
    )
  }
  return (
    <div className={styles.main}>
      {itemsList.map(item =>
        <ItemBlock
          item={item}
          counterUP={props.counterUP}
          counterDOWN={props.counterDOWN}
          />)}
    </div>
  );

}
