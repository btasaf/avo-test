import axios from 'axios';

export async function fetchItemsList(text) {
    const res = await axios.post('/api/fetchItemsList',{text:text});
    return JSON.parse(res.data.body).products
}
