import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchItemsList } from './ItemsDisplayAPI';

const initialState = {
  itemsList: [],
  status: "idle"
};

export const getItemsListAsync = createAsyncThunk(
  'items/fetchItemsList',
  async (text) => {
    const response = await fetchItemsList(text);
    return response;
  }
);

export const itemsDisplaySlice = createSlice({
  name: 'items',
  initialState,
  reducers: {
    resetList: (state,action) => {
      state.itemsList = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getItemsListAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getItemsListAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.itemsList = action.payload;
      });
  },
});

export const { resetList } = itemsDisplaySlice.actions;

export const selectItemsList = (state) => state.items.itemsList;
export const selectStatus = (state) => state.items.status;



export default itemsDisplaySlice.reducer;
