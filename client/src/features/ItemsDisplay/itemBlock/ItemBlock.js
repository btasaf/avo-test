import React, { useState } from 'react';
import styles from './ItemBlock.module.css';

export function ItemBlock(props) {
  const [counter,setCounter] = useState(0)
  const {image_url,brand,name,unit,price,sale_price} = props.item

  return (
    <div className={styles.main}>
      <div className={styles.info}>
        <div className={styles.brandAndName}>
          {brand}|
          <span>{name}</span>
        </div>
        <div className={styles.unit}>{unit}</div>
        <div className={styles.priceAndBtnCon}>
          {renderBtn()}
          {renderPrice()}
        </div>
      </div>
      <div className={styles.pic}>
        <img src={image_url} />
      </div>
    </div>
  );
  function renderPrice() {
    let sale = sale_price?"₪"+sale_price:" "
    const priceProps = {
      style:{
        textDecoration: sale_price?"line-through":"",
        color:sale_price?"#a2a7ad":"black",
      }
    }
    return(
      <div className={styles.prices}>
        <div>{sale}</div>
        <div {...priceProps}>₪{price}</div>
      </div>
    )
  }
  function renderBtn() {
    if(!counter)
    return <div onClick={() => {props.counterUP();setCounter(1)}} className={styles.btnBuy}>הוספה לעגלה</div>
    const plusBtnProps = {
      className: styles.changeBtn,
      onClick:() => {props.counterUP();setCounter(counter + 1)}
    }
    const minusBtnProps = {
      className: styles.changeBtn,
      onClick:() => {props.counterDOWN();setCounter(counter - 1)}
    }
    return(
      <div className={styles.btnCounter}>
        <div {...plusBtnProps}>+</div>
        <div className={styles.Counter}>{counter}</div>
        <div {...minusBtnProps}>-</div>
      </div>
    )

  }
}
