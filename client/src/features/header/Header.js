import React, { useState ,useEffect,useRef} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { selectLanguage,selectLanguageIndex,updateIndex } from './../language/languageSlice.js';
import styles from './Header.module.css';
import {Categories} from './../Categories/Categories.js';
import {ItemsDisplay} from './../ItemsDisplay/ItemsDisplay.js';

export function Header(props) {
  const {topLeftNew,topLeftRecommended,topLeftSale,
    topRightDeliverToTitle,topRightDeliverTo,
    logSignIn,categoriesTitle,cartTitle,
    searchPlaceHolder,cancleBtnText} = useSelector(selectLanguage).header
  const langIndex = useSelector(selectLanguageIndex)

  const dispatch = useDispatch();

  const webSearch = useRef(null);

  const [isViewOnTop,setIsViewOnTop] = useState(true)
  const [isMobItemsDisplay,setIsMobItemsDisplay] = useState(false)
  const [itemCounter,setItemCounter] = useState(0)
  const [searchInput,setSearchInput] = useState("")

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  },[]);

  useEffect(() => {
    setSearchInput("")
    setIsMobItemsDisplay(false)
  },[props.isMobile]);

  function handleScroll(e){
    setIsViewOnTop(window.scrollY === 0)
  }


  let mainConStyle = [styles.headerCon]
  if(langIndex === "heb") mainConStyle.push(styles.dirctionRTL)

  return (
    <div className={mainConStyle.join(" ")}>
      {props.isMobile?renderHeaderTopMob():renderHeaderTop()}
      {props.isMobile?renderHeaderBottomMob():renderHeaderBottom()}
    </div>
  );

  function renderHeaderTop(){
    let deliverToConStyle = [styles.headerTopRight]
    if(langIndex === "heb") deliverToConStyle.push(styles.textAlignRight)

    let headerTopCon = [styles.headerTopCon]
    if(!isViewOnTop||isMobItemsDisplay) headerTopCon.push(styles.headerTopSlide)
    if(langIndex === "heb") headerTopCon.push(styles.dirctionRTL)

    return (
      <div className={headerTopCon.join(" ")}>
        <div className={styles.headerTopLeft}>
          <img className={styles.topLeftImg} src={"assets/shopImg.png"}></img>
          <div className={styles.topLeftLinks}>{topLeftSale}</div>
          <div className={styles.seperator}></div>
          <div className={styles.topLeftLinks}>{topLeftRecommended}</div>
          <div className={styles.seperator}></div>
          <div className={styles.topLeftLinks}>{topLeftNew}</div>
        </div>
        <div className={deliverToConStyle.join(" ")}>
          {renderLangiageToggle()}
          <div className={styles.deliverCon}>
            <img className={styles.locationImg} src={"assets/location.png"}></img>
            <div>
              <div>{topRightDeliverToTitle}</div>
              <div className={styles.deliverTo}>{topRightDeliverTo}</div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  function renderHeaderBottom(){
    let listDisplayCon = <div></div>
    if(searchInput.length > 1)
      listDisplayCon = renderWebListDisplayCon()
    return (
      <div className={styles.headerBottomCon}>
        <Categories/>
        {listDisplayCon}
        {renderSearch()}
        <div className={styles.signInCartCon}>
          <div className={styles.logSignIn}>{logSignIn}</div>
          {renderCart()}
        </div>
      </div>
    )
  }

  function renderWebListDisplayCon(){
    let {bottom,height,left,right,top,width} = webSearch.current.getBoundingClientRect()
    let backDropProps = {
      onClick: () => {setSearchInput("")},
      className: styles.backDrop,
    }
    let searchInnerConProps = {
      className: styles.WebListDisplayCon,
      style:{
        top:(bottom-13)+"px",
        left:(left-2)+"px",
        maxHeight:(window.innerHeight-top-100)+"px",
        width:width+"px",
      }
    }
    return (
      <div>
        <div {...backDropProps}></div>
        <div {...searchInnerConProps}>
          <ItemsDisplay
            counterUP={() => setItemCounter(itemCounter+1)}
            counterDOWN={() => setItemCounter(itemCounter-1)}
            text={searchInput}/>
        </div>
      </div>
    )
  }

  function renderHeaderTopMob(){
    let conStyle = [styles.headerTopCon]
    conStyle.push(styles.headerTopConMob)
    if(!isViewOnTop||isMobItemsDisplay) conStyle.push(styles.headerTopSlide)
    return (
      <div className={conStyle.join(" ")}>
        <div className={styles.headerTopMobSection_A_Con}>
          <div>{topRightDeliverToTitle} <span className={styles.deliverTo}>{topRightDeliverTo}</span></div>
          <img src={"assets/arrowDownBlack.png"}/>
        </div>
        <div className={styles.headerTopMobSection_B_Con}>
          <div></div>
          <img  className={styles.headerTopMobSection_B_Conimg} src={"/assets/customerImg.png"}/>
          {renderLangiageToggle(true)}
        </div>

      </div>
    )
  }

  function renderHeaderBottomMob(){
    let conStyle = [styles.headerBottomCon]
    conStyle.push(styles.headerBottomConMob)
    let displayItemsMobCon = null
    let cancleBtn = null
    if(isMobItemsDisplay){
      cancleBtn = (
        <div
          onClick={() => {setSearchInput("");setIsMobItemsDisplay(false)}}
          className={styles.cancleBtn}>
          {cancleBtnText}
        </div>
      )
      let displayItemsMobConProps = {
        className:styles.displayItemsMobCon,
        style:{
          height: window.innerHeight - 70 +"px",
          direction: langIndex === "heb"?"rtl":"ltr",
          top: "85px"
        }
      }
      displayItemsMobCon = (
        <div {...displayItemsMobConProps}>
          <ItemsDisplay
            counterUP={() => setItemCounter(itemCounter+1)}
            counterDOWN={() => setItemCounter(itemCounter-1)}
            text={searchInput}/>
        </div>
      )
    }

    return (
      <div className={conStyle.join(" ")}>
        {renderSearch(true)}
        {cancleBtn}
        {displayItemsMobCon}
      </div>
    )
  }

  function renderLangiageToggle(){
    return(
      <div className={styles.LangToggleCon}>
        <img
          src={"assets/Flag-"+langIndex+".jpg"}
          onClick={() => dispatch(updateIndex())}/>
      </div>
    )
  }

  function renderCart(){
    let counter = null
    if(itemCounter)
      counter = <span className={styles.cartCounter}>{itemCounter}</span>
    return (
      <div className={styles.cartCon}>
        <img src={"assets/cartIcon.png"}/>
        <span>{cartTitle}</span>
        {counter}
      </div>
    )
  }

  function renderSearch(isMob){
    let searchConStyle = [styles.searchCon]
    if(isMob) searchConStyle.push(styles.searchConMob)
    if(langIndex === "heb") searchConStyle.push(styles.searchConLTR)

    const inputProps = {
      value: searchInput,
      placeHolder:searchPlaceHolder,
      // onClick: (e) => {e.stopPropagation();e.preventDefault()}
    }
    const searchConProps = {
      onFocus:() => {isMob&&setIsMobItemsDisplay(true)},
      ref:webSearch,
      className:searchConStyle.join(" "),
      onChange: (e) => setSearchInput(e.target.value)
    }
    return (
      <div {...searchConProps}>
        <input {...inputProps} />
        <img
          onClick={() => {searchInput.length&&setSearchInput("")}}
          src={"assets/"+(searchInput.length?"xIcon":"search")+".png"}/>
      </div>
    )
  }
}
