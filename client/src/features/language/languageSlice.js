import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const initialState = {
  langTree: {
    heb:{
      header:{
        topLeftNew:"חדש על המדף",
        topLeftRecommended:"מומלצים",
        topLeftSale:"מבצעי Avo",
        topRightDeliverToTitle:"נשלח אל:",
        topRightDeliverTo:"אריה בראון 8",
        logSignIn:"התחברות / הרשמה",
        categoriesTitle:"קטגוריות",
        cartTitle:"עגלה",
        searchPlaceHolder:"איזה מוצר אתם מחפשים?",
        cancleBtnText:"ביטול",
      },
      itemDisplay:{
        noItemsFoundText:"אופס... לא מצאנו מוצר כזה",
      },
      mainPage:{
        underPngText:"הזמינו לפני השעה 11:00 בימים ראשון עד חמישי ולפני השעה 8:00 ביום שישי כדי לקבל את ההזמנה באותו יום! הזמנות מאוחרות ישלחו ביום העסקים הבא",
      }
    },
    eng:{
      header:{
        topLeftNew:"New in Store",
        topLeftRecommended:"Featured",
        topLeftSale:"Avo Sales!",
        topRightDeliverToTitle:"Deliver To:",
        topRightDeliverTo:"arie brown 8",
        logSignIn:"Sign In / Sign Up",
        categoriesTitle:"BROWSE CATEGORIES",
        cartTitle:"cart",
        searchPlaceHolder:"Search Products...",
        cancleBtnText:"cancel",
      },
      itemDisplay:{
        noItemsFoundText:"Oops... Nothing was found",
      },
      mainPage:{
        underPngText:"Order by 11:00 AM (Sunday - Thursday) and by 8:00 AM on Friday for free, same-day delivery!"
      }
    }
  },
  langIndex: "heb"
};



export const languageSlice = createSlice({
  name: 'lang',
  initialState,
  reducers: {
    updateIndex: (state,action) => {
      state.langIndex = state.langIndex === "heb"?"eng":"heb";
    },
  }
});

export const { updateIndex } = languageSlice.actions;

export const selectLanguage = (state) => state.lang.langTree[state.lang.langIndex];
export const selectLanguageIndex = (state) => state.lang.langIndex;



export default languageSlice.reducer;
