import React, { useState ,useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Header } from './../header/Header.js';
import styles from './MainPage.module.css';
import {  selectLanguage } from './../language/languageSlice.js';

export function MainPage() {
  const {underPngText} = useSelector(selectLanguage).mainPage
  const [isMobile,setIsMobile] = useState(false)

  useEffect(() => {
    setIsMobile(window.innerWidth < 1025)
    window.addEventListener('resize', updateSize);
    return () => window.removeEventListener('resize', updateSize);
  },[]);

  function updateSize(){
    setIsMobile(window.innerWidth < 1025)
  }

  let underPngTextStyle = [styles.underPngText]
  if(isMobile) underPngTextStyle.push(styles.underPngTextMob)

  return (
    <div className={styles.mainCon}>
      <Header isMobile={isMobile}/>
      <img className={styles.stripImg} src={"assets/mainPageStrip"+(isMobile?"Mob":"")+".png"}></img>
      <div className={underPngTextStyle.join(" ")} >{underPngText}</div>
    </div>
  );
}
