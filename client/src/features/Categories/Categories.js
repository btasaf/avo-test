import React, { useState ,useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {  selectLanguage,selectLanguageIndex,updateIndex } from './../language/languageSlice.js';
import styles from './Categories.module.css';

export function Categories(props) {
  const {categoriesTitle} = useSelector(selectLanguage).header
  const langIndex = useSelector(selectLanguageIndex)
  const [isViewOnTop,setIsViewOnTop] = useState(true)
  const dispatch = useDispatch();

  useEffect(() => {

  },[]);

  let mainConStyle = [styles.mainCon]
  if(langIndex === "heb") mainConStyle.push(styles.dirctionRTL)

  return (
    <div className={mainConStyle.join(" ")}>
      <span>{categoriesTitle}</span>
      <img src={"assets/arrowDown.png"}/>
    </div>
  );

}
